from node:16.1.0-buster

WORKDIR /web_sdk_demo

RUN apt -y update && apt -y upgrade

COPY . .

RUN npm install
RUN npm install -g serve
RUN npm run start

EXPOSE 3000

ENTRYPOINT ["serve", "-s", "build"]
