// import * as dotenv from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
// dotenv.config({ path: '../../.env-websdk' /*, debug: true*/ })
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import reportWebVitals from './reportWebVitals'
import Context from './Context'


// module.exports = () => {
 
// const env = dotenv.config().parsed;

// const envKeys = Object.keys(env).reduce((prev, next) => {
//     prev[`process.env.${next}`] = JSON.stringify(env[next]);
//     return prev;
// }, {});

//     return {
//         plugins: [
//         new webpack.DefinePlugin(envKeys)
//         ]
//     };
// };

// console.log(process.env);  
// console.log(process.env.MIX_REACT_APP_WEB_SDK_LICENSE);

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Context>
        <App />
      </Context>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('codecorpdemo')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
