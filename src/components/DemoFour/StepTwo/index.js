import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import * as CortexDecoder from 'cortexdecoder_web'
import Modal from '../../Modal'
import { items } from './data'
import Body from './Body'
import Footer from './Footer'
import ShortUniqueId from 'short-unique-id';
import bwipjs from 'bwip-js';
import { symbology } from '../../../data/symbologies';


;(async function () {
  // const license =
  //   'Whr//4Tw5vzO74KHsrAteWwm3Aj1ZAzR21WGeo2enAJKM1Ohfl9g/oakdckrprM3UCJP55U+cey+87FTj3dVxla/+cC3O4TDRFK1Png5QCqrdpucpwefjP+m7MlPxDIt9gAhaw2E0Xko9gVjJq7ook9iN3hhLVjhXXqnkS6wEK3J/Cw0T1LVweiv4YAgI5MtYI6nQ9ArU3XGjOtdMrQzI30QYSiThv0AHYtjNbt4ZXQSoTNZLdKT4sgmV1ea00SaVubHmKLPanhdtBIn+TNBtBfRRTt26N0A6eO2Ao4jRO8PaWw/ecWtx96IftQyuF3SnCwK4pca4lZotn2oogmLUtSfM31KG9Kjnui5EWyJLDCFAXVg/uEU3uUG2BXv0B6HwDsPmOGdisPvlL31hO8fsA=='

  let license
  if (process.env.MIX_REACT_APP_WEB_SDK_LICENSE) {
    license = process.env.MIX_REACT_APP_WEB_SDK_LICENSE
  } else {
    license =
    'ZsSeYGTtoMIDLP/0LVROSodiW7qIuAo7g0iVBUqSXDGBinmPVlHyyaN3ZIzoHQEbyPGncAS8kciiVeEzXp0RvOL55oBcqCa10AduO8CEteTMgg7ofwiMvBfMP4JmN85qxqA6/HFzcWhsAoTaAG07mka4p+L1Kmq21LNZUVg5ECL6bzBtfs64x5WBs6/ECGs+UERWPBc66cxkvLq9+6KiRrYcfR4Gy1I3qTxOBL3Bbf/k/tpfsoC3iW3XHl/V7fSq3kk+NhtlVufb++mwY9Y/fKZ2nW32JzORSPg7+Hv4jthqhFnSJEZZib1nfQXDvko9EKPCZEFkx4gz5NW8JFW7qWaS1cjbGw26jq0Wpb17fpgVc/W2hSPCZtPAknViRF4sJ4rSFXgjKDUDdJfQvNapWECnJL4w4XZg/TTISqLc6Zg9L2NavL01Ydf/c+HdKJMv0wL6jXxCijPugqTo+z1WBZW6jrAsNBTx2LxcRefF0R+uq5QllIGWWx813BEAbos+6JE9gzK5uXO3mi6+xi3Aibxo+ajquuLJIhFDURgfMpE='
  }

  try {
    await CortexDecoder.CDDecoder.init()
    await CortexDecoder.CDLicense.activateLicense(license)
  } catch (e) {
    console.log(e)
  }
})()

function createRandomCode(){
  const uid = new ShortUniqueId({ length: 12 });
  let imageSymbology;
  try {
    // The return value is the canvas element
    var canvas = document.createElement("canvas")
    imageSymbology =  symbology[Math.floor(Math.random()*symbology.length)];
    bwipjs.toCanvas(canvas, {
              bcid:        imageSymbology,       // Barcode type
              text:        uid(),    // Text to encode
              scale:       2,               // 3x scaling factor
              // height:      10,              // Bar height, in millimeters
              includetext: false,            // Show human-readable text
              textxalign:  'center',        // Always good to set this
              padding: 10,
              backgroundcolor: 'FFFFFF'
          });
  } catch (e) {
      console.log(e)  
  }
  return {imgURL : canvas.toDataURL(), symbologyClassName : imageSymbology}
}

export default function StepTwo({ modalToggle, setModalToggle, next, jump }) {
  
  const [list, setList] = useState(items)

  useEffect(() => {
    // eslint-disable-next-line
    items.map((item)=>{
      let randomCode = createRandomCode()
      item.image = randomCode.imgURL
      item.className = randomCode.symbologyClassName
    })
  }, []);

  const toggleHandler = async (id) => {    
    const barcodeImage = document.querySelector(`#barcode-image-id-${id}`)
    const decodingResults = await handleDecoding(barcodeImage, 1)

    const newList = list.map((item) => {
      if (item.id === id) {
        if(item.toggle){
          let randomCode = createRandomCode()
          item.image = randomCode.imgURL
          item.className = randomCode.symbologyClassName
        }
        return { ...item, toggle: !item.toggle, decodingResults }
      }else{
        delete item["decodingResults"]
        return { ...item, toggle: false }
      }
      // return item
    })
    setList(newList)
  }

  const generateNewCode = async (id) => {
    let newList = list.map((item) => {
      if (item.id === id) {
        let randomCode = createRandomCode()
        item.image = randomCode.imgURL
        item.className = randomCode.symbologyClassName
        return item
      }else{
        return item
      }
    })
    setList(newList)
  }

  const toggleAllHandler = async () => {
    const barcodeImage = document.querySelector(`#combined-barcodes`)
    const decodingResults = await handleDecoding(barcodeImage, 4)
    const toggleAllTrue = (status) => status.toggle

    const newList = list.map((item, i) => {
      if (list.every(toggleAllTrue)) {
        return {
          ...item,
          toggle: false,
          decodingResults: { results: [decodingResults.results[i]] }
        }
      } else {
        return {
          ...item,
          toggle: true,
          decodingResults: { results: [decodingResults.results[i]] }
        }
      }
    })
    setList(newList)
  }

  const handleDecoding = async (image, amt) => {
    try {
      await CortexDecoder.CDDecoder.setBarcodesToDecode(amt, true)
      return await CortexDecoder.CDDecoder.decode(image)
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Modal
      jump={jump}
      setModalToggle={setModalToggle}
      modalToggle={modalToggle}
      body={<Body data={list} toggleHandler={toggleHandler} generateNewCode={generateNewCode}/>}
      footer={<Footer next={next} toggleAllHandler={toggleAllHandler} />}
    />
  )
}

StepTwo.propTypes = {
  modalToggle: PropTypes.bool,
  setModalToggle: PropTypes.func,
  next: PropTypes.func,
  jump: PropTypes.func
}
