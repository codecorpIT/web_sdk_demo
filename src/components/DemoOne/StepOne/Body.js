import React from 'react';
import { Link } from 'react-router-dom';
import fpoQr from './images/fpo-qr@2x.png';
// import globalStyles from '../../../globalStyles.module.scss'
// import styles from './StepOne.module.scss'

export default function Body() {
 
    return (
        <>
            <h2 className="h1 my-0">Welcome to Code WebSDK Retail Shopping Experience Demo!</h2>
            <hr />
            <p className="p">
                If you are like us, we embrace the digital world, and often we shop online and also locally from small
                business or private sellers. How many times has someone sent you a “Venmo QR Code” to pay them? Tons of
                times! But, how do you scan a code on your screen, when the device you would use to scan it is the same
                phone they sent it to? PROBLEM SOLVED. We’ve created a simple app that can capture and decode barcodes
                displayed within a web browser. Try it out!
            </p>

            <Link to="/demo-one-mobile">
            <img className="demo-one-qr-code" src={fpoQr} alt="" />
            </Link>
        </>
    );
 
}
